const express = require('express');
const app = express();
const port = process.env.PORT || 8000;
const { Tasks } = require('./models');

app.use(express.json());

app.get('/', (req, res) => {
  res.status(200).json({
    status: 'success',
    message: "Hello World"
  })
});

app.get('/tasks', async (req, res) => {
  res.status(200).json({
    status: 'success',
    data: {
      tasks: await Tasks.findAll()
    }
  })
})

app.post('/tasks', async (req, res) => {
  try {
    res.status(201).json({
      status: 'success',
      data: "halo"
    })
  }

  catch(err) {
    res.status(400).json({
      status: 'fail',
      errors: [err.message]
    })
  } 
})

app.listen(port, () => {
  console.log(`Server started at ${Date()}`);
  console.log(`Listening on port ${port}`);
}) 
